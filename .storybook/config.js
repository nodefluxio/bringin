import React from "react";
import { configure, addDecorator } from "@storybook/react";
import '!style-loader!css-loader!../public/assets/icons/ndflx-feature-icon/styles.css';
import '!style-loader!css-loader!../public/assets/icons/ndflx-sys-icon/styles.css';

const req = require.context("../src", true, /\.stories\.(js|mdx)$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

addDecorator(s => <>{s()}</>);

configure(loadStories, module, addDecorator);
