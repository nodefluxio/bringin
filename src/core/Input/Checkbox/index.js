import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Icon from "../../Icon";
import { useTheme } from "../../Theme/theme";

export default function Checkbox(props) {
  const theme = useTheme();
  const { value, checked, disable, text, onClose, subtext } = props;

  const handleChange = e => {
    const val = e.target.value;
    const { checked: check } = e.target;
    const { onChange } = props;
    onChange(val, check);
  };

  return (
    <Wrapper subtext={subtext} disable={disable} theme={theme}>
      <BaseWrapper {...props}>
        <CheckboxContainer {...props}>
          {text}
          <input
            value={value}
            onChange={handleChange}
            type="checkbox"
            checked={checked}
            disabled={disable}
          />
          <CustomCheckbox checked={checked} theme={theme}>
            {checked && (
              <div className="wrap-check">
                <div className="custom-check" />
              </div>
            )}
          </CustomCheckbox>
        </CheckboxContainer>
        {onClose && (
          <Icon className="x" fontSize="16px" onClick={!disable && onClose} />
        )}
      </BaseWrapper>
      {subtext && <div className="subtext">{subtext}</div>}
    </Wrapper>
  );
}

Checkbox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any.isRequired,
  disable: PropTypes.bool,
  checked: PropTypes.bool,
  text: PropTypes.string,
  onClose: PropTypes.func,
  subtext: PropTypes.string
};

Checkbox.defaultProps = {
  disable: false,
  checked: false,
  text: "",
  onClose: null,
  subtext: ""
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  inline-size: fit-content;
  font-family: ${props => props.theme.typography.fontFamilies.display};
  font-style: normal;
  font-weight: 500;
  color: ${props => props.theme.colors.support.main};
  ${props => props.disable && `opacity: 0.5;`}
  .subtext {
    align-self: flex-end;
    font-size: 12px;
    line-height: 18px;
    text-align: right;
    letter-spacing: 0.005em;
  }
`;

const BaseWrapper = styled.div`
  position: relative;
  display: inline-block;
  inline-size: fit-content;
  border-bottom: ${props =>
    props.text !== "" || onclick !== null
      ? `2px solid ${props.theme.colors.support.secondary} `
      : `none`};
  padding-bottom: ${props => (props.text !== "" ? `8px` : `0`)};
  padding-right: ${props => (props.text !== "" ? `32px` : `0`)};
  padding-left: ${props => (props.text !== "" ? `8px` : `0`)};
  .icon {
    position: absolute;
    margin-left: 8px;
  }
`;

const CheckboxContainer = styled.label`
  position: relative;
  padding-left: ${props => (props.text !== "" ? `32px` : `25px`)};
  padding-top: 5px;
  margin-bottom: 12px;
  cursor: pointer;
  user-select: none;
  font-size: 14px;
  line-height: 21px;
  letter-spacing: 0.005em;
  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }
  span {
    .custom-check {
      left: 20%;
      top: -5%;
      width: 40%;
      height: 75%;
      border: solid white;
      border-width: 0 2px 2px 0;
      transform: rotate(45deg);
    }
  }
  :hover input ~ span {
    background: ${props => props.theme.colors.alert.green};
    border: 2px solid ${props => props.theme.colors.alert.green};
  }
`;

const CustomCheckbox = styled.span`
  position: absolute;
  top: 2px;
  z-index: 2px;
  left: 0;
  height: 16px;
  width: 16px;
  border: 2px solid ${props => props.theme.colors.alert.neutral};
  border-radius: 2px;
  .wrap-check {
    height: 16px;
    width: 16px;
  }
  .custom-check {
    content: "";
    position: absolute;
  }
  ${props =>
    props.checked &&
    `
      border: 2px solid ${props.theme.colors.alert.green};
      background-color: ${props.theme.colors.alert.green};
    `}
`;
