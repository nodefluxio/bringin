/* eslint-disable no-alert */
import React from "react";
import styled from "styled-components";
import { storiesOf } from "@storybook/react";
import Checkbox from ".";
import { lightTheme } from "../../Theme/theme";

export default {
  title: "Bringin/Input/Checkbox",
  parameters: {
    component: Checkbox
  }
};

storiesOf("Input", module)
  .addParameters({ component: Checkbox })
  .add("Checkbox", () => (
    <Wrapper>
      <Checkbox theme={lightTheme} checked={false} />
      <Checkbox theme={lightTheme} checked={true} />
      <Checkbox
        theme={lightTheme}
        checked={false}
        text="Here goes your default text"
      />
      <Checkbox
        theme={lightTheme}
        checked={false}
        text="Here goes your default text"
        onClose={() => {
          alert("hey there!");
        }}
      />
      <Checkbox
        theme={lightTheme}
        checked={false}
        text="Here goes your default text"
        onClose={() => {
          alert("hey there!");
        }}
        subtext="subtext here"
      />
      <Checkbox
        theme={lightTheme}
        checked={false}
        text="Here goes your default text"
        onClose={() => {
          alert("hey there!");
        }}
        subtext="subtext here"
        disable
      />
    </Wrapper>
  ));

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  height: 50vh;
`;
