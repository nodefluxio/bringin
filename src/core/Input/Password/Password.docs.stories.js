import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import Password from ".";
import { lightTheme } from "../../Theme/theme";

export default {
  title: "Bringin/Input/Password",
  parameters: {
    component: Password
  }
};

storiesOf("Input", module)
  .addParameters({ component: Password })
  .add("Password", () => (
    <Fragment>
      <Password
        theme={lightTheme}
        placeholder="Please type your password..."
        id="test"
        title="Title"
        subtext="please fill this :("
      />
      <Password
        theme={lightTheme}
        placeholder="Please type your password..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        disabled
      />
      <Password
        theme={lightTheme}
        placeholder="Please type your password..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        error="Error :("
      />
    </Fragment>
  ));
