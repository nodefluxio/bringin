import React, { Fragment, useRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useTheme } from "../../Theme/theme";

function Base(props) {
  const theme = useTheme();
  const { leftIcon, rightIcon, type, onChange, id, ...defaultProps } = props;
  const inputFileRef = useRef(null);

  const refClick = e => {
    if (e.current) {
      e.current.click();
    }
  };
  return (
    <Container leftIcon={leftIcon} rightIcon={rightIcon}>
      {leftIcon}
      {type === "file" ? (
        <Fragment>
          <StyledInput
            theme={theme}
            onClick={() => refClick(inputFileRef)}
            type="text"
            disabled
            {...defaultProps}
          />
          <InputHidden
            ref={inputFileRef}
            type="file"
            {...defaultProps}
            onChange={onChange}
            id={id}
          />
        </Fragment>
      ) : (
        <StyledInput theme={theme} {...props} />
      )}
      {rightIcon}
    </Container>
  );
}

Base.propTypes = {
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  leftIcon: PropTypes.element,
  rightIcon: PropTypes.element
};

Base.defaultProps = {
  leftIcon: null,
  rightIcon: null,
  onChange: () => {}
};

const Container = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  input[type="file" i] {
    opacity: 0;
  }
  .icon:first-child {
    margin: 0px 8px;
    position: absolute;
  }
  .icon:last-child {
    margin: 0px 8px;
    position: absolute;
    right: 0;
  }
  input {
    padding-left: ${props => (props.leftIcon !== null ? `40px` : `16px`)};
    padding-right: ${props => (props.rightIcon !== null ? `40px` : `16px`)};
  }
`;

const InputHidden = styled.input`
  opacity: 0;
  position: absolute;
  pointer-events: none;
`;

const StyledInput = styled.input`
  border: 2px solid
    ${props =>
      props.error
        ? props.theme.colors.alert.red
        : props.theme.colors.border.default};
  box-sizing: border-box;
  border-radius: 8px;
  min-height: 40px;
  width: 100%;
  font-weight: 600;
  padding: 0px 16px;
  font-size: 14px;
  background-color: transparent;
  color: ${props => props.theme.colors.text.default};
  ${props =>
    props.disabled &&
    props.type === "file" &&
    `
        opacity: 0.5; 
        background: transparent;
    `}
  &:hover {
    border: 2px solid ${props => props.theme.colors.border.hover};
  }
  &:focus {
    border: 2px solid ${props => props.theme.colors.accent.secondary};
    outline: none;
  }
  &:placeholder,
  &:-webkit-input-placeholder {
    opacity: 0.5;
    font-weight: 500;
  }
`;

export default Base;
