import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import SelectInput from "react-select";
import { useTheme } from "../../Theme/theme";

export default function Select({
  option,
  label,
  error,
  name,
  value,
  onChange,
  placeholder,
  width
}) {
  const theme = useTheme();
  const setflagColor = color => ({
    alignItems: "center",
    display: "flex",
    width: "100%",
    ":after": {
      backgroundColor: color,
      content: '" "',
      position: "absolute",
      right: 10,
      display: "block",
      marginRight: 8,
      height: 10,
      width: 10
    }
  });

  const customStyles = {
    menu: provided => ({
      ...provided,
      width: "100%",
      backgroundColor: theme.colors.background.main,
      boxShadow: `1px 0px 12px ${theme.colors.support.secondary}`
    }),

    option: (styles, { data }) => {
      const style = styles;
      style.color = theme.colors.color15;
      style.backgroundColor = theme.colors.background.main;
      style.height = 40;
      style.paddingLeft = 13;
      style[":hover"] = {
        backgroundColor: theme.colors.background.secondary
      };
      style.color = theme.colors.text.default;
      style.borderBottom = `1px solid ${theme.colors.background.secondary}`;
      if (data.color) {
        return { ...style, ...setflagColor(data.color) };
      }
      return { ...style };
    },

    control: (provided, state) => ({
      ...provided,
      width: "100%",
      backgroundColor: "transparent",
      height: 40,
      borderStyle: "solid",
      borderWidth: 2,
      borderRadius: 8,
      borderColor: state.isFocused
        ? theme.colors.accent.main
        : theme.colors.border.default,
      boxShadow: "none",
      ":hover": {
        borderColor: theme.colors.support.main
      }
    }),

    singleValue: (styles, { data }) => {
      const style = styles;
      style.color = theme.colors.text.default;
      if (data.color) {
        return { ...style, ...setflagColor(data.color) };
      }
      return { ...style };
    },

    input: () => ({
      color: theme.colors.text.default
    })
  };
  const indexInput = option.findIndex(x => x.value === value);
  const handleChange = selectedOption => {
    onChange({
      target: { value: selectedOption.value, name }
    });
  };
  return (
    <WrapSelect width={width}>
      {label && <InputLabel theme={theme}>{label}</InputLabel>}
      <SelectInput
        options={option}
        styles={customStyles}
        error={error}
        value={option[indexInput]}
        name={name}
        onChange={handleChange}
        placeholder={placeholder}
        components={{
          IndicatorSeparator: () => null
        }}
      />
      {error && <InputError theme={theme}>{error}</InputError>}
    </WrapSelect>
  );
}

Select.propTypes = {
  option: PropTypes.array,
  label: PropTypes.string,
  style: PropTypes.object,
  error: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.any,
  placeholder: PropTypes.string,
  width: PropTypes.string
};

Select.defaultProps = {
  option: [],
  label: "",
  style: {},
  error: "",
  onChange: () => {},
  name: "",
  value: "",
  placeholder: "Select",
  width: "240px"
};

const WrapSelect = styled.div`
  display: block;
  min-width: 240px;
  width: ${props => props.width};
`;

const InputLabel = styled.label`
  width: 100%;
  font-size: 14px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  display: block;
  position: relative;
  color: ${props => props.theme.colors.color6};
  margin-top: 10px;
  margin-bottom: 10px;
`;

const InputError = styled(InputLabel)`
  font-size: 12px;
  font-weight: 500;
  color: ${props => props.theme.colors.color4};
`;
