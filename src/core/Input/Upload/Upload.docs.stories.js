import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import Upload from ".";
import { lightTheme } from "../../Theme/theme";

export default {
  title: "Bringin/Input/Upload Input",
  parameters: {
    component: Upload
  }
};

storiesOf("Input", module)
  .addParameters({ component: Upload })
  .add("Upload", () => (
    <Fragment>
      <Upload
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        subtext="please fill this :("
      ></Upload>
      <Upload
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        disabled
      ></Upload>
      <Upload
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        error="Error :("
      ></Upload>
    </Fragment>
  ));
