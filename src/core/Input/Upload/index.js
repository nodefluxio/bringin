import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Tooltip from "../../Tooltip";
import Base from "../Base";
import Icon from "../../Icon";
import { useTheme } from "../../Theme/theme";

function Upload(props) {
  const theme = useTheme();
  const {
    id,
    title,
    actionText,
    subtext,
    width,
    href,
    disabled,
    error,
    tooltip,
    tooltipPlace,
    tooltipType,
    onClick,
    ...defaultInputProps
  } = props;
  return (
    <TextWrapper theme={theme} width={width} disabled={disabled}>
      <Row justifyContent="space-between">
        <StyledLabel theme={theme} htmlFor={id}>
          {title}
        </StyledLabel>
        {href && (
          <StyledLink href={href} theme={theme}>
            {actionText}
          </StyledLink>
        )}
        {tooltip && (
          <Tooltip
            content={actionText}
            id={id}
            place={tooltipPlace}
            type={tooltipType}
          >
            {tooltip}
          </Tooltip>
        )}
        {onClick && (
          <StyledAction onClick={onClick} theme={theme}>
            {actionText}
          </StyledAction>
        )}
      </Row>
      <Base
        error={error}
        id={id}
        disabled={disabled}
        type="file"
        rightIcon={
          <Icon color={theme.colors.alert.neutral} className="upload" />
        }
        {...defaultInputProps}
      />
      <Row justifyContent="flex-end">
        <StyledSpan theme={theme} error={error}>
          {error || subtext}
        </StyledSpan>
      </Row>
    </TextWrapper>
  );
}

Upload.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  actionText: PropTypes.string,
  subtext: PropTypes.string,
  width: PropTypes.string,
  href: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  tooltip: PropTypes.element,
  tooltipPlace: PropTypes.string,
  tooltipType: PropTypes.string,
  onClick: PropTypes.func
};

Upload.defaultProps = {
  id: null,
  title: "",
  actionText: "",
  subtext: "",
  width: "240px",
  href: "",
  disabled: false,
  error: "",
  tooltip: null,
  tooltipPlace: "bottom",
  tooltipType: "default",
  onClick: null
};

const TextWrapper = styled.div`
  min-width: 240px;
  padding: 0px 7px;
  width: ${props => props.width};
  font-family: ${props => props.theme.typography.fontFamilies.display};
  .file-upload {
    opacity: 0;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: ${props => props.justifyContent};
`;

const StyledLabel = styled.label`
  display: block;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  letter-spacing: 0.005em;
  color: ${props => props.theme.colors.text.default};
  ${props =>
    props.disabled &&
    `
        opacity: 0.5; 
        background: transparent;
    `}
`;
const StyledSpan = styled.span`
  display: block;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  letter-spacing: 0.005em;
  color: ${props =>
    props.error
      ? props.theme.colors.alert.red
      : props.theme.colors.text.default};
`;

const StyledAction = styled.span`
  display: block;
  font-weight: 500;
  font-size: 14px;
  line-height: 21px;
  letter-spacing: 0.005em;
  cursor: pointer;
  color: ${props => props.theme.colors.accent.secondary};
`;

const StyledLink = styled.a`
  cursor: pointer;
  color: ${props => props.theme.colors.accent.secondary};
  text-decoration: none;
  font-size: 14px;
`;

export default Upload;
