import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import Input from ".";
import { lightTheme } from "../../Theme/theme";

export default {
  title: "Bringin/Input/Input",
  parameters: {
    component: Input
  }
};

storiesOf("Input", module)
  .addParameters({ component: Input })
  .add("Input", () => (
    <Fragment>
      <Input
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        subtext="please fill this :("
      ></Input>
      <Input
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        disabled
      ></Input>
      <Input
        theme={lightTheme}
        placeholder="Placeholder..."
        id="test"
        title="Title"
        link="Forgot?"
        linkAddr="https://google.com"
        subtext="please fill this :("
        error="Error :("
      ></Input>
    </Fragment>
  ));
