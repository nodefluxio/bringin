import React from "react";
import styled from "styled-components";
import { withKnobs, select } from "@storybook/addon-knobs";
import { Input, Upload, Select } from "./index";
import Icon from "../Icon";
import { ThemeProvider, darkTheme, lightTheme } from "../Theme/theme";

export default {
  title: "Input",
  decorators: [withKnobs]
};

export const TextInput = () => {
  const label = "Theme";
  const options = ["dark", "light"];
  const defaultValue = "dark";
  const groupId = "global-theme";
  const themeName = select(label, options, defaultValue, groupId);
  const theme = themeName === "dark" ? darkTheme : lightTheme;
  const timeInterval = [
    {
      label: "5s",
      value: 5000
    },
    {
      label: "25s",
      value: 25000
    },
    {
      label: "1m",
      value: 60000
    },
    {
      label: "5m",
      value: 300000
    },
    {
      label: "10m",
      value: 600000
    },

    {
      label: "15m",
      value: 900000
    }
  ];
  return (
    <ThemeProvider value={theme}>
      <Wrapper
        color={theme.colors.text.default}
        bg={theme.colors.background.main}
      >
        <Input placeholder="Placeholder..."></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          href="https://www.figma.com/"
        ></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          href="https://www.figma.com/"
          disabled
        ></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          onClick={() => {
            // eslint-disable-next-line no-alert
            alert("Clicked!");
          }}
        ></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          onClick={() => {
            // eslint-disable-next-line no-alert
            alert("Clicked!");
          }}
          error="Error message"
        ></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          tooltip="100 Object from all images on batch 03"
        ></Input>
        <Input
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          error={true}
          leftIcon={<Icon color="red" className="copy" />}
          rightIcon={<Icon color="red" className="check" />}
        ></Input>
        <Upload placeholder="Placeholder..." title="Title"></Upload>
        <Upload
          placeholder="Placeholder..."
          title="Title"
          subtext="Subtext"
          actionText="link/action"
          onClick={() => {
            // eslint-disable-next-line no-alert
            alert("Clicked!");
          }}
          error="Error message"
        ></Upload>
        <Select option={timeInterval} value={5000} />
      </Wrapper>
    </ThemeProvider>
  );
};

TextInput.story = {
  parameters: { docs: { page: null } }
};

const Wrapper = styled.div`
  padding: 5%;
  width: 100vw;
  height: fit-content;
  background: ${props => props.bg || darkTheme.colors.background.main};
  h4 {
    text-align: center;
  }
  h4,
  p {
    color: ${props => props.color || darkTheme.colors.text.default};
  }
`;
