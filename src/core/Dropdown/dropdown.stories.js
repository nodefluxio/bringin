import React from "react";
import { storiesOf } from "@storybook/react";
import Dropdown, { Menu as Item } from "./index";

const menu = (
  <div>
    <Item id="user_profile">USER PROFILE</Item>
    <Item id="activate_license">ACTIVATE LICENSE</Item>
    <Item id="logout">SIGN OUT</Item>
  </div>
);
storiesOf("Dropdown", module)
  .addParameters({ component: Dropdown })
  .add("Dropdown", () => (
    <Dropdown overlay={menu} position="center">
      Dropdown
    </Dropdown>
  ))
  .add("Icon Dropdown", () => <Dropdown overlay={menu} icon="verticalized" />);
