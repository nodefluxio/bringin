import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useTheme } from "../Theme/theme";
import Icon from "../Icon";

export default function Dropdown(props) {
  const [show, setShow] = useState(false);
  const { id, children, overlay, placement, width, className, icon } = props;
  const theme = useTheme();

  function toggleDropdown() {
    setShow(!show);
  }

  return (
    <BoxDropdown>
      <WrapButton
        show={show}
        className={className}
        theme={theme}
        onClick={toggleDropdown}
        id={id}
        iconSize={!children}
      >
        {typeof children === "string" ? <span>{children}</span> : children}
        <Icon className={icon} color={theme.colors.text.default} />
      </WrapButton>
      <WrapDropdown
        show={show}
        placement={placement}
        width={width}
        theme={theme}
      >
        {overlay}
      </WrapDropdown>
      {show && <Overlay onClick={toggleDropdown} />}
    </BoxDropdown>
  );
}

Dropdown.defaultProps = {
  placement: "right",
  className: "",
  id: "",
  icon: "drop-down",
  children: null
};

Dropdown.propTypes = {
  overlay: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
  children: PropTypes.element,
  placement: PropTypes.oneOf(["center", "left", "right"]),
  width: PropTypes.string,
  className: PropTypes.string,
  id: PropTypes.string,
  icon: PropTypes.string
};

export function Menu({ id, children, onClick, className }) {
  const theme = useTheme();
  return (
    <MenuItemList id={id} onClick={onClick} className={className} theme={theme}>
      {children}
    </MenuItemList>
  );
}

Menu.propTypes = {
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  id: PropTypes.string
};

Menu.defaultProps = {
  className: "",
  id: ""
};

const BoxDropdown = styled.div`
  width: fit-content;
`;

const positionPlacement = placement => {
  switch (placement) {
    case "center":
      return `
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;       
      `;
    case "left":
      return `
        right: unset;
        left: 0; 
      `;
    default:
      return `
        right: 0;
      `;
  }
};

const WrapButton = styled.div`
  position: relative;
  padding: ${props => (props.iconSize ? `0` : `13px 16px`)};
  border-radius: 8px;
  color: ${props => props.theme.colors.text.default};
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => (props.iconSize ? `40px` : `fit-content`)};
  height: ${props => (props.iconSize ? `40px` : `auto`)};
  background: ${props =>
    props.show
      ? props.theme.colors.support.secondary
      : props.theme.colors.background.secondary};
`;

const WrapDropdown = styled.div`
  position: relative;
  z-index: -1;
  box-sizing: border-box;
  width: ${props => props.width || `max-content`};
  height: auto;
  background-color: ${props => props.theme.colors.background.main};
  opacity: 0;
  transition: 0.1s ease-in;
  visibility: hidden;
  border-radius: 8px;
  border: 0px;
  box-shadow: 1px 0px 12px ${props => props.theme.colors.support.secondary};
  margin-top: 4px;
  ${({ show }) =>
    show &&
    `
    z-index: 9;
    opacity: 1;
    visibility: visible;
    transition: 0.2s ease-out;
  `};
  ${({ placement }) => positionPlacement(placement)};
`;

const MenuItemList = styled.div`
  width: 100%;
  text-align: left;
  color: ${props => props.theme.colors.text.default};
  font-size: 12px;
  font-weight: bold;
  box-sizing: border-box;
  font-style: normal;
  font-stretch: normal;
  line-height: 14px;
  padding: 13px;
  outline: none;
  ${({ onClick }) => onClick && `cursor: pointer;`};
  a {
    text-decoration: none;
  }
  :not(:last-child) {
    border-bottom: 1px solid ${props => props.theme.colors.support.secondary};
  }
  :hover {
    background-color: ${props => props.theme.colors.support.secondary};
    transition: 0.2s;
  }
`;

const Overlay = styled.div`
  position: fixed;
  content: "";
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
  z-index: 5;
`;
