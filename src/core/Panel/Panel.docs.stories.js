import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import Styled from "styled-components";
// import { action } from "@storybook/addon-actions";
import Panel from ".";

export default {
  title: "Bringin/Panel",
  parameters: {
    component: Panel
  },
  includeStories: []
};

storiesOf("Panel", module)
  .addParameters({ component: Panel })
  .add("Panel Docs", () => (
    <Fragment>
      <Layout>
        <Panel>Default</Panel>
        <Panel title="Small" onClose={() => {}} size="small">
          Small with close
        </Panel>
        <Panel onClose={() => {}} shadow size="large">
          Large with Shadow
        </Panel>
        <Panel title="Fluid" size="fluid">
          Fluid
        </Panel>
      </Layout>
    </Fragment>
  ));

const Layout = Styled.div`
    display:grid;
    grid-row-gap:30px;
  `;
