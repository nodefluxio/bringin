import React from "react";
import { withKnobs, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import Styled from "styled-components";

import Panel from "./index";
import { ThemeProvider, darkTheme, lightTheme } from "../Theme/theme";

export default {
  title: "Panel",
  decorators: [withKnobs]
};

const renderPanel = themeName => {
  const label = "Size";
  const options = ["default", "small", "large", "fluid"];
  const defaultValue = "default";
  const groupId = "panel-size";
  const theme = themeName === "dark" ? darkTheme : lightTheme;
  return (
    <ThemeProvider value={theme}>
      <Layout>
        <Panel size={select(label, options, defaultValue, groupId)}>
          Default
        </Panel>
        <Panel
          title="Title 1"
          size={select(label, options, defaultValue, groupId)}
        >
          Title only
        </Panel>
        <Panel
          title="Title 2"
          onClose={() => {}}
          size={select(label, options, defaultValue, groupId)}
        >
          With title and close button
        </Panel>
        <Panel
          onClose={() => {}}
          shadow
          size={select(label, options, defaultValue, groupId)}
        >
          Button close only and shadow
        </Panel>
      </Layout>
    </ThemeProvider>
  );
};

export const PanelExample = () => {
  const label = "Theme";
  const options = ["dark", "light"];
  const defaultValue = "light";
  const groupId = "global-theme";
  return renderPanel(select(label, options, defaultValue, groupId));
};

const Layout = Styled.div`
    display:grid;
    grid-row-gap:30px;
  `;
