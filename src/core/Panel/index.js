import React from "react";
import Styled from "styled-components";
import PropTypes from "prop-types";
import Icon from "../Icon";
// import { useTheme } from "../Theme/theme";

export default function Panel(props) {
  // const theme = useTheme();

  const {
    width,
    padding,
    children,
    border,
    shadow,
    title,
    onClose,
    size
  } = props;
  return (
    <Wrapper
      width={width}
      padding={padding}
      border={border}
      shadow={shadow}
      pTitle={title}
      onClose={onClose}
      size={size}
    >
      {(title || onClose) && (
        <Header>
          {title}
          {onClose && (
            <BtnClose onClick={onClose}>
              <Icon className="x" />
            </BtnClose>
          )}
        </Header>
      )}
      {children}
    </Wrapper>
  );
}

Panel.propTypes = {
  children: PropTypes.any,
  width: PropTypes.string,
  padding: PropTypes.string,
  border: PropTypes.string,
  shadow: PropTypes.bool,
  title: PropTypes.string,
  onClose: PropTypes.func,
  size: PropTypes.oneOf(["small", "default", "large", "fluid"])
};
Panel.defaultProps = {
  children: null,
  width: "100%",
  padding: "32px",
  border: `1px solid #E3E8EE`,
  shadow: false,
  title: null,
  onClose: null,
  size: "default"
};

const BtnClose = Styled.div`
  position: absolute;
  right: 16px;
  top: 50%;
  transform: translateY(-50%);
  cursor:pointer;
  img{
    width: 40px;
    filter: g
  }
`;

const Header = Styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  min-height: 39px;
  padding: 11.5px 16px;
  border-bottom: 1px solid #E3E8EE;
  font-weight: 600;
  display: flex;
  align-items: center;
`;

const Wrapper = Styled.div`
  position: relative;
  background: white;
  box-shadow: ${({ shadow }) => shadow && "1px 0px 12px rgba(22, 22, 36, 0.1)"};
  border-radius: 8px;
  width: ${({ width }) => width || "100%"};
  padding: ${({ padding }) => padding};
  border: ${({ border }) => border};
  overflow: hidden;
  color: #616871;
  ${({ pTitle }) => pTitle && `padding-top: 71px`};
  ${({ pTitle }) => !pTitle && `${Header} {border-bottom: none}`}
  *{
    box-sizing: border-box;
  }

  ${({ size }) => {
    let temp = null;
    switch (size) {
      case "small":
        temp = `max-width: 376px`;
        break;
      case "large":
        temp = `max-width: 890px`;
        break;
      case "fluid":
        temp = `max-width: 100%`;
        break;
      default:
        temp = `max-width: 696px`;
        break;
    }
    return temp;
  }}
`;
