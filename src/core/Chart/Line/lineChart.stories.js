import React from "react";
import { storiesOf } from "@storybook/react";
import LineChart from "./index";
import bigdata from "./largedata";

const data = [
  { date: "24/02/20", analytics: "FR", total: 10 },
  { date: "25/02/20", analytics: "FR", total: 12 },
  { date: "26/02/20", analytics: "FR", total: 4 }
];

const stackedData = [
  { date: "24/02/20", analytics: "FR", total: 10 },
  { date: "24/02/20", analytics: "LPR", total: 12 },
  { date: "24/02/20", analytics: "PC", total: 4 },
  { date: "25/02/20", analytics: "FR", total: 12 },
  { date: "25/02/20", analytics: "LPR", total: 1 },
  { date: "25/02/20", analytics: "PC", total: 4 },
  { date: "26/02/20", analytics: "FR", total: 6 },
  { date: "26/02/20", analytics: "LPR", total: 10 },
  { date: "26/02/20", analytics: "PC", total: 14 }
];

storiesOf("Chart/Line Chart", module)
  .addParameters({ component: LineChart })
  .add("Line", () => (
    <LineChart data={data} valueLabel="total" categoryLabel="date" />
  ))
  .add("Area", () => (
    <LineChart
      data={data}
      valueLabel="total"
      categoryLabel="date"
      area={true}
    />
  ))
  .add("Stackable Line", () => (
    <LineChart
      data={stackedData}
      valueLabel="total"
      categoryLabel="date"
      stack={true}
    />
  ))
  .add("Big Data Stackable Line", () => (
    <LineChart
      data={bigdata}
      valueLabel="total"
      categoryLabel="datetime"
      stack
    />
  ))
  .add("Stackable Area", () => (
    <LineChart
      data={stackedData}
      valueLabel="total"
      categoryLabel="date"
      stack={true}
      area={true}
    />
  ));
