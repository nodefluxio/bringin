import React from "react";
import PropTypes from "prop-types";
import ReactEcharts from "echarts-for-react";
import { useTheme } from "../../Theme/theme";
import color from "../color";

export default function LineChart(props) {
  const {
    data,
    title,
    valueLabel,
    categoryLabel,
    subtitle,
    xLabelPosition,
    stack,
    area
  } = props;
  const yLabelArray = data.map(value => value[categoryLabel]);
  const distinctCategoryLabel = [
    ...new Set(data.map(value => value[categoryLabel]))
  ];
  const disctinctFlag = yLabelArray.length === distinctCategoryLabel.length;
  const leftoverKeys = Object.keys(data[0]).find(
    key => key !== categoryLabel && key !== valueLabel && key !== "color"
  );
  const distinctLegendLabel = [
    ...new Set(data.map(value => value[leftoverKeys]))
  ];

  const theme = useTheme();

  const options = {
    title: {
      text: title,
      subtext: subtitle
    },
    legend: {
      type: "scroll",
      show: !disctinctFlag,
      padding: 0,
      icon: "circle"
    },
    tooltip: {
      trigger: "item",
      axisPointer: {
        type: "shadow"
      }
    },
    grid: {
      left: "3%",
      right: "4%",
      bottom: "3%",
      containLabel: true,
      borderColor: "none"
    },
    color,
    xAxis: {
      type: "category",
      boundaryGap: false,
      position: xLabelPosition,
      data: distinctCategoryLabel,
      nameTextStyle: {
        color: theme.colors.text.default
      },
      axisLine: {
        lineStyle: {
          color: theme.colors.text.default,
          width: 0
        }
      },
      axisTick: false
    },
    yAxis: {
      type: "value",
      splitNumber: 3,
      axisLine: {
        lineStyle: {
          color: theme.colors.text.default,
          width: 0
        }
      },
      axisTick: false
    }
  };

  options.series = !disctinctFlag
    ? distinctLegendLabel.map((label, index) => ({
        type: "line",
        smooth: true,
        name: label,
        stack,
        areaStyle: area
          ? {
              color: {
                type: "linear",
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [
                  {
                    offset: 0,
                    color: color[index]
                  },
                  {
                    offset: 1,
                    color: "white"
                  }
                ],
                global: false
              }
            }
          : { opacity: 0 },
        data: data
          .filter(value => value[leftoverKeys] === label)
          .map(value => ({
            value: value[valueLabel]
          }))
      }))
    : {
        type: "line",
        smooth: true,
        areaStyle: area
          ? {
              color: {
                type: "linear",
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [
                  {
                    offset: 0,
                    color: color[0]
                  },
                  {
                    offset: 1,
                    color: "white"
                  }
                ],
                global: false
              }
            }
          : { opacity: 0 },
        data: data.map(value => ({
          value: value[valueLabel]
        }))
      };
  return <ReactEcharts option={options} opts={{ renderer: "canvas" }} />;
}

LineChart.propTypes = {
  data: PropTypes.array.isRequired,
  valueLabel: PropTypes.string.isRequired,
  categoryLabel: PropTypes.string.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  xLabelPosition: PropTypes.oneOf(["top", "bottom"]),
  stack: PropTypes.bool,
  area: PropTypes.bool
};

LineChart.defaultProps = {
  xLabelPosition: "bottom",
  title: "",
  subtitle: "",
  stack: false,
  area: false
};
