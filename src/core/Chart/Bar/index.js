import React from "react";
import PropTypes from "prop-types";
import ReactEcharts from "echarts-for-react";
import { useTheme } from "../../Theme/theme";
import color from "../color";

export default function BarChart(props) {
  const {
    data,
    title,
    valueLabel,
    categoryLabel,
    subtitle,
    xLabelPosition,
    direction,
    stack
  } = props;
  const yLabelArray = data.map(value => value[categoryLabel]);
  const distinctCategoryLabel = [
    ...new Set(data.map(value => value[categoryLabel]))
  ];
  const disctintFlag = yLabelArray.length === distinctCategoryLabel.length;
  const theme = useTheme();
  const leftoverKeys = Object.keys(data[0]).find(
    key => key !== categoryLabel && key !== valueLabel && key !== "color"
  );
  const distinctLegendLabel = [
    ...new Set(data.map(value => value[leftoverKeys]))
  ];

  const options = {
    title: {
      text: title,
      subtext: subtitle
    },
    tooltip: {
      trigger: "item",
      axisPointer: {
        type: "shadow"
      }
    },
    grid: {
      left: "3%",
      right: "4%",
      bottom: "3%",
      containLabel: true,
      borderColor: theme.colors.text.default
    },
    color,
    xAxis: {
      type: direction === "horizontal" ? "value" : "category",
      splitNumber: 3,
      position: xLabelPosition,
      axisTick: false,
      axisLine: {
        lineStyle: {
          color: theme.colors.text.default,
          width: 0
        }
      }
    },
    yAxis: {
      type: direction === "horizontal" ? "category" : "value",
      splitNumber: 3,
      axisTick: false,
      axisLine: {
        lineStyle: {
          color: theme.colors.text.default,
          width: 0
        }
      }
    }
  };

  if (direction === "horizontal") {
    options.yAxis.data = distinctCategoryLabel;
  } else {
    options.xAxis.data = distinctCategoryLabel;
  }

  options.series = !disctintFlag
    ? distinctLegendLabel.map((label, index) => ({
        type: "bar",
        barMaxWidth: 8,
        name: label,
        stack,
        data: data
          .filter(value => value[leftoverKeys] === label)
          .map(value => ({
            value: value[valueLabel],
            name: value.analytics,
            itemStyle: {
              color: value.color,
              barBorderRadius:
                // eslint-disable-next-line no-nested-ternary
                index === distinctLegendLabel.length - 1
                  ? direction === "horizontal"
                    ? [0, 5, 5, 0]
                    : [5, 5, 0, 0]
                  : 0
            }
          }))
      }))
    : {
        type: "bar",
        barMaxWidth: 8,
        data: data.map(value => ({
          value: value[valueLabel],
          itemStyle: {
            color: value.color,
            barBorderRadius:
              direction === "horizontal" ? [0, 5, 5, 0] : [5, 5, 0, 0]
          }
        }))
      };

  return <ReactEcharts option={options} opts={{ renderer: "svg" }} />;
}

BarChart.propTypes = {
  data: PropTypes.array.isRequired,
  valueLabel: PropTypes.string.isRequired,
  categoryLabel: PropTypes.string.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  xLabelPosition: PropTypes.oneOf(["top", "bottom"]),
  stack: PropTypes.bool,
  direction: PropTypes.oneOf(["horizontal", "vertical"])
};

BarChart.defaultProps = {
  xLabelPosition: "bottom",
  title: "",
  subtitle: "",
  stack: false,
  direction: "horizontal"
};
