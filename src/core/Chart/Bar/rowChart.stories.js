import React from "react";
import { withKnobs, select } from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import BarChart from "./index";

storiesOf("Chart/Bar Chart", module)
  .addParameters({ component: BarChart })
  .addDecorator(withKnobs)
  .add("Row", () => {
    const data = [
      { date: "24/02/20", analytics: "FR", total: 10, color: "#132a1f" },
      { date: "24/02/20", analytics: "LPR", total: 12, color: "#ff12e4" },
      { date: "24/02/20", analytics: "PC", total: 4, color: "#005512" }
    ];

    return (
      <BarChart
        data={data}
        valueLabel="total"
        categoryLabel="analytics"
        xLabelPosition="top"
      />
    );
  })
  .add("Bar", () => {
    const data = [
      { date: "24/02/20", analytics: "FR", total: 10, color: "#132a1f" },
      { date: "24/02/20", analytics: "LPR", total: 12, color: "#ff12e4" },
      { date: "24/02/20", analytics: "PC", total: 4, color: "#005512" }
    ];

    return (
      <BarChart
        data={data}
        valueLabel="total"
        categoryLabel="analytics"
        direction="vertical"
      />
    );
  })
  .add("Stackable Row", () => {
    const stackOption = [true, false];
    const data = [
      { date: "24/02/20", analytics: "FR", total: 10, color: "#F36B86" },
      { date: "24/02/20", analytics: "LPR", total: 12, color: "#FFD15C" },
      { date: "24/02/20", analytics: "PC", total: 4, color: "#84C041" },
      { date: "25/02/20", analytics: "FR", total: 12, color: "#132a1f" },
      { date: "25/02/20", analytics: "LPR", total: 1, color: "#ff12e4" },
      { date: "25/02/20", analytics: "PC", total: 4, color: "#005512" },
      { date: "26/02/20", analytics: "FR", total: 6, color: "#2F8E76" },
      { date: "26/02/20", analytics: "LPR", total: 10, color: "#272848" },
      { date: "26/02/20", analytics: "PC", total: 14, color: "#216353" }
    ];

    return (
      <BarChart
        data={data}
        valueLabel="total"
        categoryLabel="date"
        xLabelPosition="top"
        stack={select("Stack", stackOption, true, "stack")}
      />
    );
  })
  .add("Stackable Bar", () => {
    const stackOption = [true, false];
    const data = [
      { date: "24/02/20", analytics: "FR", total: 10, color: "#F36B86" },
      { date: "24/02/20", analytics: "LPR", total: 12, color: "#FFD15C" },
      { date: "24/02/20", analytics: "PC", total: 4, color: "#84C041" },
      { date: "25/02/20", analytics: "FR", total: 12, color: "#132a1f" },
      { date: "25/02/20", analytics: "LPR", total: 1, color: "#ff12e4" },
      { date: "25/02/20", analytics: "PC", total: 4, color: "#005512" },
      { date: "26/02/20", analytics: "FR", total: 6, color: "#2F8E76" },
      { date: "26/02/20", analytics: "LPR", total: 10, color: "#272848" },
      { date: "26/02/20", analytics: "PC", total: 14, color: "#216353" }
    ];

    return (
      <BarChart
        data={data}
        valueLabel="total"
        categoryLabel="date"
        xLabelPosition="top"
        direction="vertical"
        stack={select("Stack", stackOption, true, "stack")}
      />
    );
  });
