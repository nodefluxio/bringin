import React from "react";
import { storiesOf } from "@storybook/react";
import PieChart from "./index";

const data = [
  { date: "24/02/20", analytics: "FR", total: 10 },
  { date: "25/02/20", analytics: "FR", total: 12 },
  { date: "26/02/20", analytics: "FR", total: 4 }
];

storiesOf("Chart/Pie Chart", module)
  .addParameters({ component: PieChart })
  .add("Pie", () => (
    <PieChart data={data} valueLabel="total" categoryLabel="date" />
  ))
  .add("Donut", () => (
    <PieChart
      data={data}
      valueLabel="total"
      categoryLabel="date"
      donut={true}
    />
  ));
