import React from "react";
import PropTypes from "prop-types";
import ReactEcharts from "echarts-for-react";
import { useTheme } from "../../Theme/theme";
import color from "../color";

export default function PieChart(props) {
  const {
    data,
    title,
    valueLabel,
    categoryLabel,
    subtitle,
    donut,
    legendPosition
  } = props;
  const yLabelArray = data.map(value => value[categoryLabel]);
  const theme = useTheme();

  const options = {
    title: {
      text: title,
      subtext: subtitle
    },
    tooltip: {
      trigger: "item",
      formatter: "{a}: <br/>{b} : {c} ({d}%)"
    },
    color,
    legend: {
      type: "scroll",
      textStyle: { color: theme.colors.text.default },
      orient:
        legendPosition === "right" || legendPosition === "left"
          ? "vertical"
          : "horizontal",
      data: yLabelArray
    },
    series: {
      type: "pie",
      name: categoryLabel,
      radius: donut ? ["40%", "55%"] : "55%",
      data: data.map(value => ({
        value: value[valueLabel],
        name: value[categoryLabel]
      }))
    }
  };

  if (legendPosition === "bottom") {
    options.legend.bottom = 0;
  } else if (legendPosition === "right") {
    options.legend.right = 0;
  } else if (legendPosition === "left") {
    options.legend.left = 0;
  }

  return <ReactEcharts option={options} opts={{ renderer: "svg" }} />;
}

PieChart.propTypes = {
  data: PropTypes.array.isRequired,
  valueLabel: PropTypes.string.isRequired,
  categoryLabel: PropTypes.string.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  legendPosition: PropTypes.oneOf(["top", "bottom", "rigt", "left"]),
  donut: PropTypes.bool
};

PieChart.defaultProps = {
  legendPosition: "top",
  title: "",
  subtitle: "",
  donut: false
};
