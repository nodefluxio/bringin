import React, { Fragment, useState } from "react";
import ReactTooltip from "react-tooltip";
import styled from "styled-components";
import PropTypes from "prop-types";
import { useTheme } from "../Theme/theme";

const Tooltip = ({ id, content, type, place, solid, multiline, children }) => (
  <Fragment>
    <div data-tip data-for={id}>
      {content}
    </div>
    <CustomTooltip theme={useTheme()} type={type}>
      <ReactTooltip id={id} place={place} solid={solid} multiline={multiline}>
        {children}
      </ReactTooltip>
    </CustomTooltip>
  </Fragment>
);

Tooltip.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.array.isRequired,
  content: PropTypes.object,
  type: PropTypes.oneOf(["default", "accent"]),
  place: PropTypes.oneOf(["right", "bottom", "left", "top"]),
  solid: PropTypes.bool,
  multiline: PropTypes.bool
};

Tooltip.defaultProps = {
  content: {},
  type: "default",
  place: "right",
  solid: true,
  multiline: false
};

export default Tooltip;

const mainStyle = (type, theme) => {
  switch (type) {
    case "accent":
      return `
        color: ${theme.colors.text.default};
        box-shadow: 0 0 4px 0 ${theme.colors.accent.secondary};
        background-color: ${theme.colors.accent.main};
      `;
    default:
      return `
        color: ${theme.colors.support.secondary};
        box-shadow: 0 0 4px 0 ${theme.colors.support.main};
        background-color: ${theme.colors.support.main};
      `;
  }
};

const arrowStyle = (type, theme) => {
  switch (type) {
    case "accent":
      return `
        .__react_component_tooltip.type-dark.place-right:after {
          border-right-color: ${theme.colors.accent.secondary};
        }
        .__react_component_tooltip.type-dark.place-left:after {
          border-left-color: ${theme.colors.accent.secondary};
        }
        .__react_component_tooltip.type-dark.place-bottom:after {
          border-bottom-color: ${theme.colors.accent.secondary};
        }
        .__react_component_tooltip.type-dark.place-top:after {
          border-top-color: ${theme.colors.accent.secondary};
        }
      `;
    default:
      return `
        .__react_component_tooltip.type-dark.place-right:after {
          border-right-color: ${theme.colors.support.main};
        }
        .__react_component_tooltip.type-dark.place-left:after {
          border-left-color: ${theme.colors.support.main};
        }
        .__react_component_tooltip.type-dark.place-bottom:after {
          border-bottom-color: ${theme.colors.support.main};
        }
        .__react_component_tooltip.type-dark.place-top:after {
          border-top-color: ${theme.colors.support.main};
        }
      `;
  }
};

const CustomTooltip = styled.div`
  .__react_component_tooltip.type-dark {
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 17px;
    letter-spacing: 0.5px;
    ${props => mainStyle(props.type, props.theme)}
  }
  ${props => arrowStyle(props.type, props.theme)}
`;
