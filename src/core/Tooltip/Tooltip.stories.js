import React from "react";
import { withKnobs, select } from "@storybook/addon-knobs";
import Tooltip from "./index";

export default {
  title: "Tooltip",
  decorators: [withKnobs]
};

export const TooltipExample = () => {
  const place = {
    label: "Place",
    type: ["right", "bottom", "left", "top"],
    default: "right",
    id: "place-type"
  };
  const type = {
    label: "Theme Type",
    type: ["default", "accent"],
    default: "default",
    id: "theme-type"
  };
  return (
    <Tooltip
      content={
        <div data-tip data-for="tes">
          Hover ke sini beb :*
        </div>
      }
      id="tes"
      place={select(place.label, place.type, place.default, place.id)}
      type={select(type.label, type.type, type.default, type.id)}
      solid
      multiline
    >
      Only use letters, number, and ‘_’ .<br /> You can change it anytime
    </Tooltip>
  );
};

TooltipExample.story = {
  parameters: { docs: { page: null } }
};
