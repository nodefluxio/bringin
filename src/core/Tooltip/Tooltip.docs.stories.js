import React, { Fragment } from "react";
import styled from "styled-components";
import { storiesOf } from "@storybook/react";
import Tooltip from ".";

export default {
  title: "Bringin/Tooltip",
  parameters: {
    component: Tooltip
  },
  includeStories: []
};

storiesOf("Tooltip", module)
  .addParameters({ component: Tooltip })
  .add("Tooltip Docs", () => {
    const place = ["right", "bottom", "left", "top"];
    return (
      <Fragment>
        {place.map((val, key) => (
          <Tooltip
            content={<div>Hover ke sini beb :* | Type : ${val}</div>}
            id={`tes${key}`}
            place={val}
            solid
            multiline
          >
            Tooltip {val} | Only use letters, number, and ‘_’ .<br /> You can
            change it anytime
          </Tooltip>
        ))}
      </Fragment>
    );
  });
