import React from "react";
import styled from "styled-components";
import { withKnobs, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import Button from "./index";
import Icon from "../Icon";
import { ThemeProvider, darkTheme, lightTheme } from "../Theme/theme";

export default {
  title: "Button",
  decorators: [withKnobs]
};

const renderButton = themeName => {
  const label = "Type";
  const options = ["default", "accent", "ghost", "float"];
  const defaultValue = "default";
  const groupId = "button-type";
  const theme = themeName === "dark" ? darkTheme : lightTheme;
  return (
    <ThemeProvider value={theme}>
      <Wrapper
        color={theme.colors.text.default}
        bg={theme.colors.background.main}
      >
        <h4>
          Accent: {defaultValue}, theme: {themeName}{" "}
        </h4>
        <p>Size: default</p>
        <Button
          onClick={action("clicked")}
          type={select(label, options, defaultValue, groupId)}
        >
          Button
        </Button>
        <p>Size: small</p>
        <Button
          onClick={action("clicked")}
          size="small"
          type={select(label, options, defaultValue, groupId)}
        >
          Button
        </Button>
        <p>Size: full</p>
        <Button
          onClick={action("clicked")}
          size="full"
          type={select(label, options, defaultValue, groupId)}
        >
          Button
        </Button>
        <p>Size: icon</p>
        <Button
          onClick={action("clicked")}
          size="icon"
          type={select(label, options, defaultValue, groupId)}
        >
          <Icon fontSize="32px" color="red" className="icon icon-copy" />
        </Button>
        <p>Disabled: true</p>
        <Button
          onClick={action("clicked")}
          disabled={true}
          type={select(label, options, defaultValue, groupId)}
        >
          Button
        </Button>
      </Wrapper>
    </ThemeProvider>
  );
};

export const ButtonExamples = () => {
  const label = "Theme";
  const options = ["dark", "light"];
  const defaultValue = "dark";
  const groupId = "global-theme";
  return renderButton(select(label, options, defaultValue, groupId));
};

ButtonExamples.story = {
  parameters: { docs: { page: null } }
};

const Wrapper = styled.div`
  padding: 5%;
  width: 100vw;
  height: fit-content;
  background: ${props => props.bg || darkTheme.colors.background.main};
  h4 {
    text-align: center;
  }
  h4,
  p {
    color: ${props => props.color || darkTheme.colors.text.default};
  }
  button {
    margin-bottom: 30px;
  }
`;
