import React, { Fragment } from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Button from ".";
import { lightTheme } from "../Theme/theme";

export default {
  title: "Bringin/Button",
  parameters: {
    component: Button
  },
  includeStories: []
};

storiesOf("Button", module)
  .addParameters({ component: Button })
  .add("Button Docs", () => (
    <Fragment>
      <Button onClick={action("clicked")} theme={lightTheme}>
        Button
      </Button>
      <Button onClick={action("clicked")} type="accent" theme={lightTheme}>
        Button
      </Button>
      <Button onClick={action("clicked")} size="icon" theme={lightTheme}>
        <span role="img" aria-label="so cool">
          😀
        </span>
      </Button>
      <Button
        onClick={action("clicked")}
        size="icon"
        type="ghost"
        theme={lightTheme}
      >
        <span role="img" aria-label="so cool">
          😀
        </span>
      </Button>

      <Button
        onClick={action("clicked")}
        size="icon"
        type="float"
        theme={lightTheme}
      >
        <span role="img" aria-label="so cool">
          😀
        </span>
      </Button>
    </Fragment>
  ));
