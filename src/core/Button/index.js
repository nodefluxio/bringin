import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { useTheme, darkTheme } from "../Theme/theme";

function Button(props) {
  const theme = useTheme();
  const {
    children,
    disabled,
    type,
    size,
    leftIcon,
    rightIcon,
    onClick,
    id,
    className
  } = props;
  return (
    <StyledButton
      id={id}
      className={className}
      theme={theme}
      onClick={onClick}
      disabled={disabled}
      type={type}
      size={size}
    >
      <LeftIcon>{leftIcon}</LeftIcon>
      {children}
      <RightIcon>{rightIcon}</RightIcon>
    </StyledButton>
  );
}
Button.propTypes = {
  theme: PropTypes.object.isRequired,
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(["default", "ghost", "float", "accent"]),
  size: PropTypes.oneOf(["default", "icon", "small", "full"]),
  disabled: PropTypes.bool,
  rightIcon: PropTypes.element,
  leftIcon: PropTypes.element,
  id: PropTypes.string,
  className: PropTypes.string
};
Button.defaultProps = {
  size: "default",
  type: "default",
  disabled: false,
  rightIcon: null,
  leftIcon: null,
  onClick: () => {},
  id: "",
  className: ""
};
const borderRadDef = `8px`;
const smallGap = `8px`;
const backgroundDef = `transparent`;
const heightDef = `40px`;
const typeResult = (type, disabled, theme) => {
  const borderColor = theme.colors.border.default;
  const accentColor1 = theme.colors.accent.main;
  const accentColor2 = theme.colors.accent.secondary;
  let color = theme.colors.text.default;
  let border = `2px solid ${borderColor}`;
  let borderRad = borderRadDef;
  let background = `transparent`;
  let bgHover = `transparent`;
  let borderHover = `2px solid ${disabled ? borderColor : accentColor1}`;
  let bgActive = `transparent`;
  let borderActive = `2px solid ${borderColor}`;
  switch (type) {
    case "ghost":
      border = `none`;
      borderRad = `0px`;
      bgHover = disabled ? backgroundDef : borderColor;
      borderHover = `none`;
      bgActive = backgroundDef;
      borderActive = `none`;
      break;
    case "float":
      border = `none`;
      background = borderColor;
      bgHover = `transparent`;
      borderHover = disabled ? borderColor : theme.colors.background.secondary;
      bgActive = `transparent`;
      borderActive = borderColor;
      break;
    case "accent":
      color = darkTheme.colors.text.default;
      border = `none`;
      background = accentColor2;
      bgHover = disabled ? accentColor2 : accentColor1;
      borderHover = `none`;
      bgActive = accentColor2;
      borderActive = `none`;
      break;
    default:
      color = theme.colors.text.default;
      border = `2px solid ${borderColor}`;
      borderRad = borderRadDef;
      background = `transparent`;
      bgHover = `transparent`;
      borderHover = `2px solid ${disabled ? borderColor : accentColor1}`;
      bgActive = `transparent`;
      borderActive = `2px solid ${borderColor}`;
  }
  return `
    border: ${border};
    border-radius: ${borderRad};
    background: ${background};
    color: ${color};
    &:hover {
      background: ${bgHover};
      border: ${borderHover};
    }
    &:active {
      background: ${bgActive}
      border: ${borderActive};
    }
  `;
};
const sizeResult = size => {
  let minWidth = heightDef;
  let width = `auto`;
  let height = heightDef;
  let padding = `0px 16px`;
  switch (size) {
    case "icon":
      width = heightDef;
      padding = `0px`;
      break;
    case "small":
      minWidth = `32px`;
      height = `32px`;
      padding = `0px ${smallGap}`;
      break;
    case "full":
      minWidth = `100%`;
      width = `100%`;
      break;
    default:
      minWidth = heightDef;
      width = `auto`;
      height = heightDef;
      padding = `0px 16px`;
  }
  return `
    min-width: ${minWidth};
    width: ${width};
    height: ${height};
    padding: ${padding};
  `;
};
const StyledButton = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  background: ${backgroundDef};
  font-size: 12px;
  font-weight: 600;
  cursor: ${props => (props.disabled ? "default" : `pointer`)};
  &:disabled {
    opacity: 0.5;
  }
  ${props => typeResult(props.type, props.disabled, props.theme)}
  ${props => sizeResult(props.size)}
`;
const RightIcon = styled.div`
  padding-left: ${smallGap};
`;
const LeftIcon = styled.div`
  padding-right: ${smallGap};
`;

export default Button;
