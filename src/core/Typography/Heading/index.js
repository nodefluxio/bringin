import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useTheme } from "../../Theme/theme";
import Base from "../Base";
import { colorStyling } from "../helper";

export default function Heading(props) {
  const theme = useTheme();
  const { tag } = props;
  return <TextComponent as={tag} theme={theme} {...props} />;
}

Heading.propTypes = {
  tag: PropTypes.oneOf(["h1", "h2", "h3"]),
  case: PropTypes.oneOf(["uppercase", "lowercase", "capitalize", "none"]),
  weight: PropTypes.oneOf(["500", "600"]),
  align: PropTypes.oneOf(["left", "center", "right", "justify"]),
  color: PropTypes.oneOf([
    "default",
    "accent",
    "red",
    "yellow",
    "green",
    "neutral"
  ])
};

Heading.defaultProps = {
  tag: "h1",
  case: "none",
  weight: "600",
  align: "left",
  type: "default",
  color: "default"
};

const typeStyling = tag => {
  switch (tag) {
    case "h1":
      return `18px`;
    case "h2":
      return `16px`;
    case "h3":
      return `14px`;
    default:
      return null;
  }
};

const TextComponent = styled(Base)`
  font-size: ${props => typeStyling(props.type)};
  font-weight: ${props => props.weight};
  line-height: 1.6;
  color: ${props => colorStyling(props.theme, props.color)};
`;
