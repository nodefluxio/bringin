import React from "react";
import { withKnobs, select } from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import Heading from "./index";

storiesOf("Typography", module)
  .addParameters({ component: Heading })
  .addDecorator(withKnobs)
  .add("Heading", () => {
    const tag = ["h1", "h2", "h3"];
    const weight = ["500", "600"];
    const color = ["default", "accent", "red", "yellow", "green", "neutral"];
    return (
      <Heading
        tag={select("Tag", tag, "h1", "text-tag")}
        weight={select("Weight", weight, "500", "text-weight")}
        color={select("Color", color, "default", "text-color")}
      >
        Heading Test
      </Heading>
    );
  });
