import React from "react";
import Styled from "styled-components";
import { useTheme } from "../../Theme/theme";

function Base() {
  const theme = useTheme();
  return <BaseText theme={theme} />;
}

const BaseText = Styled.span`
font-family: ${props => props.theme.typography.fontFamilies.display};
font-stretch: normal;
line-height: normal;
font-style: normal;
`;

export default Base;
