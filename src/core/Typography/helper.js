export const colorStyling = (theme, color) => {
  switch (color) {
    case "accent":
      return theme.colors.accent.secondary;
    case "red":
      return theme.colors.alert.red;
    case "yellow":
      return theme.colors.alert.yellow;
    case "green":
      return theme.colors.alert.green;
    case "neutral":
      return theme.colors.alert.neutral;
    case "default":
    default:
      return theme.colors.text.default;
  }
};
