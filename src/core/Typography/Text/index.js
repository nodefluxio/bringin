import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useTheme } from "../../Theme/theme";
import Base from "../Base";
import { colorStyling } from "../helper";

export default function Text(props) {
  const theme = useTheme();
  const { tag } = props;
  return <TextComponent as={tag} theme={theme} {...props} />;
}

Text.propTypes = {
  tag: PropTypes.oneOf(["span", "p"]),
  case: PropTypes.oneOf(["uppercase", "lowercase", "capitalize", "none"]),
  weight: PropTypes.oneOf(["500", "600", "700"]),
  align: PropTypes.oneOf(["left", "center", "right", "justify"]),
  type: PropTypes.oneOf(["default", "small", "xsmall"]),
  color: PropTypes.oneOf([
    "default",
    "accent",
    "red",
    "yellow",
    "green",
    "neutral"
  ])
};

Text.defaultProps = {
  tag: "span",
  case: "none",
  weight: "500",
  align: "left",
  type: "default",
  color: "default"
};

const typeStyling = type => {
  switch (type) {
    case "small":
      return `12px`;
    case "xsmall":
      return `11px`;
    default:
      return `14px`;
  }
};

const TextComponent = styled(Base)`
  font-size: ${props => typeStyling(props.type)};
  font-weight: ${props => props.weight};
  line-height: 1.6;
  color: ${props => colorStyling(props.theme, props.color)};
`;
