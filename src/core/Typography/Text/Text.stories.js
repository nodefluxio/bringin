import React from "react";
import { withKnobs, select } from "@storybook/addon-knobs";
import { storiesOf } from "@storybook/react";
import Text from "./index";

storiesOf("Typography", module)
  .addParameters({ component: Text })
  .addDecorator(withKnobs)
  .add("Text", () => {
    const type = ["default", "small", "xsmall"];
    const weight = ["500", "600", "700"];
    const color = ["default", "accent", "red", "yellow", "green", "neutral"];
    return (
      <Text
        type={select("Type", type, "default", "text-type")}
        weight={select("Weight", weight, "500", "text-weight")}
        color={select("Color", color, "default", "text-color")}
      >
        Used for multiline pieces of content. Lorem ipsum dolor sit amet, ex
        lucilius hendrerit vim, tempor scaevola iudicabit ei ius, te eum illud
        impetus antiopam. Eu wisi commune volutpat pro, usu at alii magna
        aperiam.
      </Text>
    );
  });
