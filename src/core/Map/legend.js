import { useLeaflet } from "react-leaflet";
import L from "leaflet";
import { useEffect } from "react";
import { useTheme } from "../Theme/theme";
import { shadeHexColor, getColorShade } from "./helper";

const Legend = props => {
  const theme = useTheme();
  const { data, dataKey, step } = props;
  const { map } = useLeaflet();

  useEffect(() => {
    const valueKey = Object.keys(data[0]).filter(
      key => key.toLowerCase() !== dataKey.toLowerCase()
    );
    let lowest = Number.POSITIVE_INFINITY;
    let highest = Number.NEGATIVE_INFINITY;
    let tmp;
    for (let i = data.length - 1; i >= 0; i -= 1) {
      tmp = data[i][valueKey[0]];
      if (tmp < lowest) lowest = tmp;
      if (tmp > highest) highest = tmp;
    }
    const stepValue = Math.ceil((highest - lowest) / step);

    const legend = L.control({ position: "bottomright" });

    legend.onAdd = () => {
      const div = L.DomUtil.create("div", "info legend");
      const labels = [];
      let from;
      let to;

      for (let i = 1; i <= step; i += 1) {
        from = lowest + stepValue * (i - 1);
        to = lowest + stepValue * i;

        labels.push(
          `<i style="background:${shadeHexColor(
            theme.colors.accent.main,
            getColorShade(from, lowest, highest, step)
          )}"></i> ${from}${to ? `&ndash;${to}` : "+"}`
        );
      }

      div.innerHTML = labels.join("<br>");
      return div;
    };

    legend.addTo(map);
  }, []);
  return null;
};

export default Legend;
