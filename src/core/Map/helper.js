export function shadeHexColor(color, percent) {
  const f = parseInt(color.slice(1), 16);
  const t = percent < 0 ? 0 : 255;
  const p = percent < 0 ? percent * -1 : percent;
  // eslint-disable-next-line no-bitwise
  const R = f >> 16;
  // eslint-disable-next-line no-bitwise
  const G = (f >> 8) & 0x00ff;
  // eslint-disable-next-line no-bitwise
  const B = f & 0x0000ff;
  return `#${(
    0x1000000 +
    (Math.round((t - R) * p) + R) * 0x10000 +
    (Math.round((t - G) * p) + G) * 0x100 +
    (Math.round((t - B) * p) + B)
  )
    .toString(16)
    .slice(1)}`;
}

export function getColorShade(value, lowest, highest, step) {
  const stepValue = Math.ceil((highest - lowest) / step);
  let group = (step + 1) / 10;
  for (let i = 1; i <= step; i += 1) {
    if (
      lowest + stepValue * (i - 1) <= parseInt(value, 10) &&
      parseInt(value, 10) < lowest + stepValue * i
    ) {
      group = (step - i) / 10;
    }
  }
  return group;
}
