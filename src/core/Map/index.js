import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import Styled from "styled-components";
import L from "leaflet";
import { Map as LeafletMap, TileLayer, GeoJSON, Marker } from "react-leaflet";
import Control from "@skyeer/react-leaflet-custom-control";
import { useTheme } from "../Theme/theme";
import { shadeHexColor, getColorShade } from "./helper";
import Legend from "./legend";

export default function Map(props) {
  const theme = useTheme();
  const geoJSONMap = useRef(null);
  const mapRef = useRef(null);
  const [controlKey, setControlKey] = useState("");
  const [controlValue, setControlValue] = useState("");
  const {
    latitude,
    longitude,
    zoom,
    geojson,
    tileLayer,
    noTile,
    markerPosition,
    markerIcon,
    markerClick,
    data,
    dataKey,
    step,
    title
  } = props;

  let lowest = Number.POSITIVE_INFINITY;
  let highest = Number.NEGATIVE_INFINITY;
  const valueKey = data
    ? Object.keys(data[0]).filter(
        key => key.toLowerCase() !== dataKey.toLowerCase()
      )
    : "";

  function colorRenderer(value) {
    return {
      fillColor: shadeHexColor(
        theme.colors.accent.main,
        getColorShade(value.properties.value, lowest, highest, step)
      ),
      weight: 2,
      opacity: 1,
      color: "white",
      dashArray: "3",
      fillOpacity: 0.7
    };
  }

  function highlightFeature(e) {
    const layer = e.target;
    const selectedKeys = Object.keys(layer.feature.properties).filter(
      key => key.toLowerCase() === dataKey.toLowerCase()
    );
    setControlKey(layer.feature.properties[selectedKeys[0]]);
    setControlValue(layer.feature.properties.value || 0);
    layer.setStyle({
      weight: 2,
      color: theme.colors.support.main,
      dashArray: "",
      fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
      layer.bringToFront();
    }
  }

  function resetHighlight(e) {
    setControlKey("");
    geoJSONMap.current.leafletElement.resetStyle(e.target);
  }

  function zoomToFeature(e) {
    const map = mapRef.current.leafletElement;
    map.fitBounds(e.target.getBounds());
  }

  function onEachFeature(feature, layer) {
    layer.on({
      mouseover: highlightFeature,
      mouseout: resetHighlight,
      click: zoomToFeature
    });
  }

  function geoJSONRenderer() {
    let arrayGeoJSON = [];
    const { features } = geojson;
    let combinedData = [];
    if (data) {
      let tmp;
      for (let i = data.length - 1; i >= 0; i -= 1) {
        tmp = data[i][valueKey[0]];
        if (tmp < lowest) lowest = tmp;
        if (tmp > highest) highest = tmp;
      }
    }
    if (features.length > 0 && data) {
      const selectedKeys = Object.keys(features[0].properties).filter(
        key => key.toLowerCase() === dataKey.toLowerCase()
      );
      combinedData = features.map(feature => {
        const index = data.findIndex(
          datum =>
            datum[dataKey].toLowerCase() ===
            feature.properties[selectedKeys[0]].toLowerCase()
        );
        const newObject = Object.assign({}, feature);
        if (index !== -1) {
          newObject.properties.value = data[index][valueKey[0]];
        }
        return newObject;
      });
    }
    if (Object.prototype.toString.call(geojson).indexOf("Object") > -1) {
      arrayGeoJSON.push(geojson);
    } else if (Object.prototype.toString.call(geojson).indexOf("Array") > -1) {
      arrayGeoJSON = geojson;
    }
    return arrayGeoJSON.map(value => (
      <GeoJSON
        ref={geoJSONMap}
        key={value}
        data={value}
        style={combinedData.length > 0 ? colorRenderer : {}}
        onEachFeature={combinedData.length > 0 ? onEachFeature : () => {}}
      />
    ));
  }

  function markerRenderer() {
    let arrayMarker = [];
    const defaultIcon = new L.Icon.Default();
    if (Object.prototype.toString.call(markerPosition).indexOf("Object") > -1) {
      arrayMarker.push(markerPosition);
    } else if (
      Object.prototype.toString.call(markerPosition).indexOf("Array") > -1
    ) {
      arrayMarker = markerPosition;
    }
    return arrayMarker.map(value => (
      <Marker
        key={value.lat + value.long}
        position={[value.lat, value.long]}
        icon={markerIcon || defaultIcon}
        onClick={() => (markerClick !== null ? markerClick(value) : {})}
      />
    ));
  }

  return (
    <MapWrapper theme={theme}>
      <LeafletMap center={[latitude, longitude]} zoom={zoom} ref={mapRef}>
        <TileLayer url={tileLayer} opacity={noTile ? 0 : 1} />
        {markerPosition && markerRenderer()}
        {geojson && geoJSONRenderer()}
        {data && geojson && <Legend {...props} />}
        {data && title && geojson && (
          <Control position="topright" className="info">
            <div>
              <h4>{title}</h4>
              {controlKey ? (
                <p>
                  {controlKey} : {controlValue}
                </p>
              ) : (
                <p>Hover {dataKey}</p>
              )}
            </div>
          </Control>
        )}
      </LeafletMap>
    </MapWrapper>
  );
}

const MapWrapper = Styled.div`
.leaflet-container {
  background: ${props => props.theme.colors.background.main}
}
.info {
  max-width: 200px;
  padding: 8px;
  font: 14px/16px "Barlow", Arial, Helvetica, sans-serif;
  background: ${props => props.theme.colors.background.main};
  box-shadow: 0 0 12px ${props => props.theme.colors.support.secondary};
  border-radius: 8px;
}
.info h4 {
  margin: 0 0 5px;
  color: ${props => props.theme.colors.text.default};;
}
.legend {
  line-height: 18px;
  color: ${props => props.theme.colors.text.default};
}
.legend i {
  width: 18px;
  height: 18px;
  float: left;
  margin-right: 8px;
  opacity: 0.7;
}
`;

Map.propTypes = {
  latitude: PropTypes.string.isRequired,
  longitude: PropTypes.string.isRequired,
  geojson: PropTypes.oneOfType([
    PropTypes.shape({
      lat: PropTypes.string,
      long: PropTypes.string
    }),
    PropTypes.array
  ]),
  markerPosition: PropTypes.array,
  zoom: PropTypes.number,
  tileLayer: PropTypes.string,
  noTile: PropTypes.bool,
  markerIcon: PropTypes.object,
  markerClick: PropTypes.func,
  data: PropTypes.array,
  dataKey: PropTypes.string,
  step: PropTypes.number,
  title: PropTypes.title
};

Map.defaultProps = {
  zoom: undefined,
  geojson: null,
  tileLayer: "http://{s}.tile.osm.org/{z}/{x}/{y}.png",
  noTile: false,
  markerIcon: null,
  markerPosition: null,
  markerClick: null,
  data: null,
  dataKey: "",
  step: 5,
  title: ""
};
