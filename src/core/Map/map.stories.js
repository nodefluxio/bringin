import React from "react";
import { storiesOf } from "@storybook/react";
import L from "leaflet";
import Map from "./index";
import sample from "./sample.json";
import { data } from "./data";

const customIcon = new L.Icon({
  iconUrl: "https://leafletjs.com/examples/custom-icons/leaf-green.png",
  iconRetinaUrl: "https://leafletjs.com/examples/custom-icons/leaf-green.png",
  iconSize: [38, 95], // size of the icon
  shadowSize: [50, 64], // size of the shadow
  iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62], // the same for the shadow
  popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

const markerPosition = [
  {
    lat: "-6.2614957",
    long: "106.8216503"
  },
  {
    lat: "-6.2604957",
    long: "106.8216503"
  },
  {
    lat: "-6.2614957",
    long: "106.8206503"
  }
];

storiesOf("Map", module)
  .addParameters({ component: Map })
  .add("Simple Map", () => (
    <Map latitude="-6.2614957" longitude="106.8216503" zoom={17} />
  ))
  .add("Map with GeoJson", () => (
    <Map
      latitude="-6.2095712"
      longitude="106.8494778"
      zoom={11}
      geojson={sample}
      noTile
    />
  ))
  .add("Map with GeoJson and Data", () => (
    <Map
      latitude="-6.2095712"
      longitude="106.8494778"
      zoom={11}
      geojson={sample}
      data={data}
      dataKey="kecamatan"
      title="Kepadatan Kendaraan Tiap Daerah per 15 Menit"
      noTile
    />
  ))
  .add("Map with Marker", () => (
    <Map
      latitude="-6.2614957"
      longitude="106.8216503"
      zoom={17}
      markerPosition={{
        lat: "-6.2614957",
        long: "106.8216503"
      }}
    />
  ))
  .add("Map with Multiple Marker", () => (
    <Map
      latitude="-6.2614957"
      longitude="106.8216503"
      zoom={17}
      markerPosition={markerPosition}
    />
  ))
  .add("Map with Custom Marker", () => (
    <Map
      latitude="-6.2614957"
      longitude="106.8216503"
      zoom={17}
      markerPosition={markerPosition}
      markerIcon={customIcon}
      // eslint-disable-next-line no-alert
      markerClick={response => alert(response)}
    />
  ));
