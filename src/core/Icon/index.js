import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

function Icon(props) {
  const { className, color, fontSize, onClick } = props;
  return (
    <StyledIcon
      className={`icon icon-${className}`}
      color={color}
      fontSize={fontSize}
      onClick={onClick}
    />
  );
}

Icon.propTypes = {
  className: PropTypes.string.isRequired,
  color: PropTypes.string,
  fontSize: PropTypes.string,
  onClick: PropTypes.func
};

Icon.defaultProps = {
  color: "",
  fontSize: "24px",
  onClick: () => {}
};

const StyledIcon = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  color: ${props => props.color};
  cursor: ${props => props.onClick && "pointer"};
  &:before {
    font-size: ${props => props.fontSize};
  }
`;

export default Icon;
