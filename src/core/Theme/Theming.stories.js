import React from "react";
import styled from "styled-components";
import { withKnobs, select, text } from "@storybook/addon-knobs";
import Button from "../Button";
import { ThemeProvider, darkTheme, lightTheme } from "./theme";

export default {
  title: "Custom Theming",
  decorators: [withKnobs]
};

const renderComponent = themeName => {
  const label = "Change Border Color";
  const defaultValue = "red";
  const groupId = "border_default";
  let theme = "";
  const customTheme = {
    ...darkTheme,
    colors: {
      ...darkTheme.colors,
      border: {
        default: text(label, defaultValue, groupId)
      }
    }
  };
  if (themeName === `dark`) {
    theme = darkTheme;
  }
  if (themeName === `light`) {
    theme = lightTheme;
  }
  if (themeName === `custom`) {
    theme = customTheme;
  }

  return (
    <ThemeProvider value={theme}>
      <Wrapper bg={theme.colors.background.main}>
        <Button>Hello Button</Button>
      </Wrapper>
    </ThemeProvider>
  );
};

export const ThemeExample = () => {
  const label = "Theme";
  const options = ["dark", "light", "custom"];
  const defaultValue = "dark";
  const groupId = "theme";
  return renderComponent(select(label, options, defaultValue, groupId));
};

const Wrapper = styled.div`
  padding: 5%;
  width: 100vw;
  height: fit-content;
  background: ${props => props.bg || darkTheme.colors.background.main};
`;
