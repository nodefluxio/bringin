import dark from "./src/dark-theme";
import light from "./src/light-theme";
import { setTheme } from "./src/utilities";

export {
  default as ThemeContext,
  ThemeProvider,
  ThemeConsumer
} from "./src/ThemeContext";
export { default as withTheme } from "./src/withTheme";
export { default as useTheme } from "./src/useTheme";

export const darkTheme = setTheme(dark);
export const lightTheme = setTheme(light);
