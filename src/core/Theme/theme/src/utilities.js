export const setTheme = theme => {
  /**
   * This object are functional colors being used the default theme.
   * A required property is required by the default theme NOT by Evergreen itself.
   */
  const colors = {
    /**
     * Available on Pane as `<Pane background="color1" />`
     * @property {string} background.color1 - Primary background color.
     * @property {string} background.color2 - Occasionally used background color
     */
    background: {
      main: theme.palette.background.color1,
      secondary: theme.palette.background.color2
    },

    /**
     * Available on Pane as `<Pane accent="color1" />`
     * @property {string} accent.color1 - Primary accent color.
     * @property {string} accent.color2 - Occasionally used accent color
     */
    accent: {
      main: theme.palette.accent.color1,
      secondary: theme.palette.accent.color2
    },

    support: {
      main: theme.palette.support.color1,
      secondary: theme.palette.support.color2
    },

    /**
     * Available on Pane as `<Pane borderBottom borderRight="muted" />`
     * @property {string} text.default - Required property.
     * @property {string} text.muted - Slightly lighter color than default. Required property.
     */
    border: {
      default: theme.palette.support.color2
    },

    /**
     * Text colors available on Text as `<Text color="default" />`.
     * @property {string} text.muted - Required property.
     */
    text: {
      default: theme.palette.support.color1
    },

    /**
     * Icon colors available on Icon.
     * @property {string} icon.default - Required property.
     */
    icon: {
      default: theme.palette.support.color1
    },

    alert: {
      red: theme.palette.alert.red,
      yellow: theme.palette.alert.yellow,
      green: theme.palette.alert.green,
      neutral: theme.palette.support.color1
    }
  };

  return {
    colors,
    typography: theme.typography
  };
};
