export default {
  background: {
    color1: "#FFFFFF",
    color2: "#FAFAFA"
  },
  accent: {
    color1: "#2F8E76",
    color2: "#216353"
  },
  support: {
    // Use only for text and icon, limited on Dark Theme
    color1: "#616871",
    // Use only for border, line, disabled background state, hovered component background, background, limited on Dark Theme
    color2: "#E3E8EE"
  },
  alert: {
    red: "#F36B86",
    yellow: "#FFD15C",
    green: "#84C041",
    neutral: "#616871"
  }
};
