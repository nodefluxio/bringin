export default {
  background: {
    color1: "#161624",
    color2: "#191932"
  },
  accent: {
    color1: "#2F8E76",
    color2: "#216353"
  },
  support: {
    // Use only for text and icon, limited on Dark Theme
    color1: "#F0F3FF",
    // Use only for border, line, disabled background state, hovered component background, background, limited on Dark Theme
    color2: "#272848"
  },
  alert: {
    red: "#F36B86",
    yellow: "#FFD15C",
    green: "#84C041",
    neutral: "#F0F3FF"
  }
};
