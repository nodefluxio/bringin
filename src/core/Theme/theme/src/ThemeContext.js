import React from "react";
import lightTheme from "./light-theme";
import { setTheme } from "./utilities";

/**
 * Use React 16.3+ createContext API.
 */

const selectedTheme = setTheme(lightTheme);
const ThemeContext = React.createContext(selectedTheme);
const { Provider: ThemeProvider, Consumer: ThemeConsumer } = ThemeContext;

export default ThemeContext;
export { ThemeProvider, ThemeConsumer };
