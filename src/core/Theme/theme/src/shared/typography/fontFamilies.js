export default {
  /**
   * @property {string} display - Used for headings larger than 20px.
   */
  display: `"Barlow", sans-serif`,

  /**
   * @property {string} ui - Used for text and UI (which includes almost anything).
   */
  ui: `"Barlow", sans-serif`
};
