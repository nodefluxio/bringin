import React from "react";
import PropTypes from "prop-types";
import Styled from "styled-components";

const DEFAULT_GRID_LAYOUT = 24;
export default function Col({ span, children, className }) {
  const percentage = (span / DEFAULT_GRID_LAYOUT) * 100;
  return (
    <WrapContent
      span={span}
      percentage={percentage}
      className={`${className} nodeflux-col-${span}`}
    >
      {children}
    </WrapContent>
  );
}

Col.propTypes = {
  span: PropTypes.number,
  className: PropTypes.string,
  children: PropTypes.object
};

Col.defaultProps = {
  span: 24,
  children: {}
};

const WrapContent = Styled.div`
    position: relative;
    flex: 0 1 auto;
    max-width: 100%;
    min-height: 1px;
    ${({ span, percentage }) =>
      span &&
      `
       &.nodeflux-col-${span}{
        display: block;
        flex: 0 0 ${percentage}%;
        max-width: ${percentage}%;        
       }
    `};
`;
