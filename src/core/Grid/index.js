import React from "react";
import PropTypes from "prop-types";
import Styled from "styled-components";

export default function Row({
  onClick,
  overflow,
  justify,
  align,
  direction,
  children
}) {
  return (
    <WrapCol
      onClick={onClick}
      overflow={overflow}
      justify={justify}
      align={align}
      direction={direction}
    >
      {children}
    </WrapCol>
  );
}

Row.propTypes = {
  children: PropTypes.object,
  onClick: PropTypes.string,
  overflow: PropTypes.string,
  justify: PropTypes.string,
  align: PropTypes.string,
  direction: PropTypes.string
};

const WrapCol = Styled.div`
    display: flex;
    align-content: flex-start;
    flex-flow: row wrap;
    ${({ onClick }) => onClick && `cursor: pointer;`};
    ${({ overflow }) => overflow && `overflow: ${overflow};`}
    ${({ justify }) => justify && `justify-content: ${justify};`}
    ${({ align }) => align && `align-items: ${align};`}
    ${({ direction }) => direction && `flex-direction: ${direction};`}
`;
