import React from "react";
import { storiesOf } from "@storybook/react";
import Styled from "styled-components";
import Row from ".";
import Col from "./Col";

export default {
  title: "Bringin/Grid",
  parameters: {
    component: Row
  },
  includeStories: []
};

const WrapRow = Styled.div`
    .box-custom {
      border: 1px solid green;
      box-sizing: border-box;
      text-align: center;
      margin: 10px 0;
    }
  `;

storiesOf("Grid", module)
  .addParameters({ component: Row })
  .add("Grid Docs", () => (
    <WrapRow>
      <Row>
        <Col className="box-custom" span={24}>
          col-24
        </Col>
      </Row>
      <Row>
        <Col className="box-custom" span={12}>
          col-12
        </Col>
        <Col className="box-custom" span={12}>
          col-12
        </Col>
      </Row>
      <Row>
        <Col className="box-custom" span={8}>
          col-8
        </Col>
        <Col className="box-custom" span={8}>
          col-8
        </Col>
        <Col className="box-custom" span={8}>
          col-8
        </Col>
      </Row>
      <Row>
        <Col className="box-custom" span={6}>
          col-6
        </Col>
        <Col className="box-custom" span={6}>
          col-6
        </Col>
        <Col className="box-custom" span={6}>
          col-6
        </Col>
        <Col className="box-custom" span={6}>
          col-6
        </Col>
      </Row>
      <Row>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
        <Col className="box-custom" span={4}>
          col-4
        </Col>
      </Row>
    </WrapRow>
  ));
