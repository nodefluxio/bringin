# Project Title

Bringin Design System - Nodeflux

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
NodeJS 12x
npm 6x
```

### Installing

```
npm install
```

### Running Storybook

```
npm run storybook
```

### Build Storybook

```
npm run build-storybook
```

### Build Package Bringin

```
npm run build-core
```

### Publish Package to NPM

```
npm run publish
```

## Running the tests

```
npm run test
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [ReactJS 16.12](http://www.reactjs.org)
* [Storybook 16.12](https://www.storybook.js.org/)
* [Styled Component 5.0](https://styled-components.com)


## Authors

* **Bagas Satria Nugroho** - *Initial work* - [bsatria](https://github.com/bsatria)
* **Nabilla Tsuraya Ilmi** - *Initial work* - [nabs]
* **Bhagas Adi Nugroho** - *Initial work* - [bahagasn]


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details