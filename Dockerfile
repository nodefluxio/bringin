# build stage
FROM node:12.13.0-alpine as build-env
ENV NODE_ENV=production
RUN \ 
  apk update && \
  apk add git python make gcc g++

ADD . /opt/app
WORKDIR /opt/app

RUN yarn install 
RUN yarn run build-storybook

FROM nginx:alpine
RUN apk add --no-cache nodejs-current yarn jq bash
#RUN yarn global add dotenv-to-json @ethical-jobs/dynamic-env
COPY --from=build-env /opt/app/storybook-static /var/www
#COPY --from=build-env /opt/app/.env.example /var/www/.env.example
COPY --from=build-env /opt/app/default.conf /etc/nginx/conf.d/default.conf
WORKDIR /var/www
EXPOSE 80
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]