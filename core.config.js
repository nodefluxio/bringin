const path = require("path");

module.exports = {
  mode: "production",
  target: "web",
  entry: "./src/core/index.js",
  optimization: {
    minimize: true
  },
  output: {
    path: path.resolve(__dirname, "./packages/core"),
    filename: "index.js",
    libraryTarget: "commonjs"
  },
  externals: {
    react: "react",
    "react-dom": "react-dom",
    "styled-components": "styled-components"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "./src/core"),
        exclude: /(node_modules|bower_components|packages|.test.[j]s|.stories.[j]s|__snapshots__)/,
        loader: "babel-loader",
        options: {
          babelrc: false,
          extends: path.join(`${__dirname}/babel.config.js`)
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  }
};
